# Drupal gallery widget

### usage
```html
<div id="widget" class="registry-search-widget" apiKey="PUBLIC_API_KEY"  registryUrl="PUBLIC_REGISTRY_URL" apiEndpoint="PUBLIC_API_ENDPOINT" />
<script src="WIDGET_SOURCE_JS_URL"></script>
<script>
    var link = document.createElement( "link" );
    link.href = "WIDGET_SOURCE_CSS_URL"
    link.type = "text/css";
    link.rel = "stylesheet";
    link.media = "screen,print";
    document.getElementsByTagName( "head" )[0].appendChild( link );
</script>
```

### Screenshots
...todo
