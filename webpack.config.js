var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');
var path = require('path');


var BUILD_DIR = path.resolve(__dirname, 'public');
var APP_DIR = path.resolve(__dirname, 'src/');
var SASS_DIR = path.resolve(__dirname, 'src/scss');

var getDEvTool = function () {
    if (process.env.NODE_ENV === 'production') {
        return 'cheap-module-source-map';
    } else {
        return 'eval';
    }
}

var config = {
    devtool: getDEvTool(),
    entry: [
        'webpack/hot/only-dev-server',
        'webpack/hot/dev-server',
        APP_DIR + '/app.jsx',
    ],
    node: { fs: 'empty' },
    output: {
        path: BUILD_DIR,
        filename: 'js/bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.(jpg|png|gif|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=img/[name].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
            {
                test: /\.html/i,
                loader: 'file?name=/[name].[ext]'
            },
            {
                test: /\.scss/i,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader'),
                include: SASS_DIR
            },
            {
                test: /\.js?/,
                include: APP_DIR,
                loader: 'babel-loader',
                query: {
                    presets: ["es2015", "react", "stage-0"]
                }
            }
        ],
    },
    postcss: [autoprefixer({browsers: ['last 2 versions']})],
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin('css/styles.css'),
        new webpack.optimize.UglifyJsPlugin({compress: {
            warnings: false
        }, minimize: process.env.NODE_ENV === 'production'}),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ],
    devServer: {
        inline: true,
        watch: true,
        https: false,
        colors: true,
        contentBase: './public',
        watchOptions: {
            poll: true
        },
    }
};

module.exports = config;
