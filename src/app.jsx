'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import values from 'lodash.values';
import forIn from 'lodash.forin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Plants from './service/plants.js'
import loki from 'lokijs';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import injectTapEventPlugin from 'react-tap-event-plugin';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import ContentAddCircle from 'material-ui/svg-icons/content/add-circle';
import ContentRemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import Chip from 'material-ui/Chip';
import MapsMap from 'material-ui/svg-icons/maps/map';
import ActionInfoOutline from 'material-ui/svg-icons/action/info-outline';
import FlatButton from 'material-ui/FlatButton';
import {CardText,Card} from 'material-ui/Card';

injectTapEventPlugin();

require('./scss/main.scss');
require('./templates/index.html');

//TODO: ugly huge component - needs refactoring (later...)
export default class Widget extends React.Component {

    constructor(props) {
        super(props);

        this.apiEndpoint = document.getElementById('widget').getAttribute('apiEndpoint');
        this.apiKey = document.getElementById('widget').getAttribute('apiKey');
        this.registryUrl = document.getElementById('widget').getAttribute('registryUrl');
        this.plantsService = new Plants(this.apiEndpoint);
        var db = new loki('plants-db');
        this.plants = db.addCollection('plants');
        this.tableData = [];

        this.state = {
            searchInProgress: false,
            filter: {
                search: '',
                sort: this.getDefaultSort(),
                tag: '_ALL_'
            },
            visibleRows: [],
            scrollableHeight: 500,
            maxTableHeight: 600
        }
    }

    getDefaultSort() {
        return [
            {
                sortBy: 'name_lat',
                sortOrder: 'asc'
            }
        ];
    }

    componentWillUnmount() {
    }

    loadGalleryData() {
    }

    getVisibleRows = (startFromPx) => {
        let startRow = Math.round(startFromPx / this.rowHeight, 0) - this.rowsOffset;
        if (startRow < 0) {
            startRow = 0;
        }
        let endRow = startRow + this.rowsPerPage + this.rowsOffset;
        console.log('tbl lng: ', this.state.tableData.length);
        if (endRow > (this.state.tableData.length - 1)) {
            endRow = this.state.tableData.length - 1;
        }
        return {start: startRow, end: endRow};
    }

    handleScroll = (e) => {
        this.refreshTableRows(e.target.scrollTop);
    }

    refreshTableRows(scroll) {
        this.state.scrollableHeight = this.state.tableData.length * this.rowHeight;
        let boundaries = this.getVisibleRows(scroll);
        this.state.visibleRows = this.state.tableData.slice(boundaries.start, boundaries.end + 1).map((row, idx) => {
            return this.getSingleRow(row, idx, boundaries);
        });
        this.setState(this.state);
    }

    componentDidMount() {
        this.checkWebp();
        this.plantsService.getCollection()
            .then((plants) => {
                //disable that WWworker for now
                // if (window.Worker) {
                //     this.myWorker = new Worker("js/worker.js");
                //     this.myWorker.postMessage(['init', values(plants)]);
                //     this.myWorker.onmessage = this.onWorkerSearchresult
                // } else {
                this.plants.insert(values(plants), {
                    unique: ['id']
                });
                // }
                this.state.tableData = this.updateSearch()//this.plants.chain().simplesort('name_lat').data(); //TODO initial sort param

                this.tableHeight = 500;
                this.rowHeight = 64;
                this.rowsOffset = 10;
                this.rowsPerPage = this.tableHeight / this.rowHeight;
                this.state.scrollableHeight = this.state.tableData.length * this.rowHeight;
                //document.getElementById('table-body').style.height = this.scrollableHeight;
                // document.querySelector('.table-body').addEventListener('scroll', this.handleScroll);
                // console.log()

                this.setState(this.state);
                this.refreshTableRows(0);
            })
    }

    onWorkerSearchresult = (e) => { //??
        console.log('search complete ', new Date());
        let result = e.data;
        this.state.tableData = result;
        this.state.searchInProgress = false;
        this.setState(this.state);
    }

    checkWebp() {
        var img = new Image();
        img.onload = () => {
            var webp = !!(img.height > 0 && img.width > 0);
            this.setState({webp: webp});
        };
        img.onerror = () => {
            this.state.webp = false
            this.setState(this.state);
        };
        img.src = 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=';
    }

    getRowStyle = (boundaries, idx) => {
        return {
            fontFamily: 'arial',
            top: (boundaries.start + idx) * this.rowHeight,
            position: 'absolute',
            width: '100%',
            height: 64,
            padding: 4,
            backgroundColor: idx % 2 === 0 ? 'transparent' : 'rgb(250, 250, 250)',
            borderBottom: 'none'
        }
    }

    getSingleRow = (row, idx, boundaries) => {
        let styles = {
            czechName: {
                fontSize: '1.2em',
                float: 'left',
                clear: 'both',
            },
            noCzechName: {
                opacity: 0.3,
                float: 'left',
                clear: 'both',
            },
            latinName: {
                float: 'left',
                clear: 'both',
                fontSize: '0.9em',
                fontStyle: 'italic',
                color: 'gray',
                lineHeight: '1.2em'
            },
            rowStyle: {
                height: 64,
                padding: 4
            },
            familyRow: {
                float: 'left',
                clear: 'both'
            },
            familyLabel: {
                float: 'left'
            },
            faimlyName: {
                paddingLeft: 5,
                float: 'left'
            },
            familyNameLatin: {
                fontStyle: 'italic',
                color: 'gray'
            },
            tagStyleFruit: {
                marginBottom: 5,
                backgroundColor: '#CDDC39'
            },
            tagStyleBloom: {
                marginBottom: 5,
                backgroundColor: '#ff9800'
            },
            tagLabel: {
                lineHeight: '24px'
            },
            tagColumn: {
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column'
            },
            infoColumn: {
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row'
            },
            semiTransparentIcon: {
                paddingRight: 5
            }
        }
        return (
            <div className="row" style={this.getRowStyle(boundaries, idx)} adjustForCheckbox={false}>
                <div className="col-sm-8">
                    <div style={styles.latinName}>{row.name_lat}</div>
                    {row.name ? <div style={styles.czechName}>{row.name}</div> :
                        <div style={styles.noCzechName}>Nemá české jméno</div>}
                    <div style={styles.familyRow}>
                        <div style={styles.familyLabel}>Čeleď:</div>
                        {row.family_name ?
                            <div style={styles.faimlyName}><span>{row.family_name}</span> (<span
                                style={styles.familyNameLatin}>{row.family_name_lat}</span>)</div> :
                            <span style={styles.familyNameLatin}>{row.family_name_lat}</span>}</div>
                </div>
                <div style={styles.tagColumn} className="col-sm-2">
                    {row.tags.map((tagId) => {
                        if (tagId === 11) {
                            return <Chip labelStyle={styles.tagLabel} style={styles.tagStyleBloom}>
                                Právě kvete
                            </Chip>
                        }
                        if (tagId === 185) {
                            return <Chip labelStyle={styles.tagLabel} style={styles.tagStyleFruit}>
                                Právě plodí
                            </Chip>
                        }
                    })}
                </div>
                <div style={styles.infoColumn} className="col-sm-2">
                    {(row.content_length > 100) ?
                        <ActionInfoOutline onClick={() => {
                            this.openRegistry(row)
                        }} title="Dočtete se víc" className="semiTransparentIcon"
                                           style={styles.semiTransparentIcon}/>
                        :
                        ''}
                    <MapsMap onClick={() => {
                        this.openRegistry(row)
                    }} title="Zobrazit na mapě" className="semiTransparentIcon"
                             style={styles.semiTransparentIcon}/>
                </div>
            </div>
        )
    }

    openRegistry(plant) {
        window.open(this.registryUrl + '/map/plants/' + plant.id, '_blank');
    }

    getSortParams = () => {
        let params = [];
        forIn(this.state.filter.sort, (sortItem) => {
            if (sortItem.sortOrder)
                params.push([sortItem.sortBy, sortItem.sortOrder === 'desc' ? true : false]);
        })
        return params;
    }

    updateSearch = () => {
        this.state.searchInProgress = true;
        this.setState(this.state);

        if (this.myWorker) {
            console.log('launching search ', new Date());
            this.myWorker.postMessage(['search', this.state.filter]);
            console.log('launching search-end ', new Date());
            return;
        }
        let chain = this.plants.chain();
        chain = chain.where((obj) => {
            let matchSearch = true;
            if (this.state.filter.search) {
                let search = this.state.filter.search.toLowerCase();
                matchSearch = obj.name.indexOf(search) !== -1 ||
                    obj.name_norm.indexOf(search) !== -1;
            }
            let matchTag = true;
            if (this.state.filter.tag && this.state.filter.tag !== '_ALL_') {
                matchTag = obj.tags.indexOf(this.state.filter.tag) > -1
            }
            return matchTag && matchSearch;
        })
        this.state.tableData = chain.compoundsort(this.getSortParams()).data();
        this.state.searchInProgress = false;
        this.setState(this.state);
        this.refreshTableRows(document.querySelector('.table-body').scrollTop);
    }

    onSearchChange = (ev) => {
        this.state.filter.search = ev.target.value;
        this.updateSearch();
    }

    onSortByChange = (sortIdx, value) => {
        this.state.filter.sort[sortIdx].sortBy = value;
        this.updateSearch();
    }

    onSortOrderChange = (sortIdx, value) => {
        this.state.filter.sort[sortIdx].sortOrder = value;
        this.updateSearch();
    }

    onAddNewSortFilter = () => {
        if (this.state.filter.sort[0].sortBy.indexOf('name') !== -1) {
            this.state.filter.sort.push({
                sortBy: 'family_name_lat',
                sortOrder: 'asc'
            })
        } else {
            this.state.filter.sort.push({
                sortBy: 'name_lat',
                sortOrder: 'asc'
            })
        }
        this.setState(this.state);
    }

    onRemoveLastSortFilter = () => {
        this.state.filter.sort.pop();
        this.setState(this.state);
    }

    clearFilter = () => {
        this.state.filter.search = '';
        this.state.filter.sort = this.getDefaultSort();
        this.state.filter.tag = '_ALL_';
        this.updateSearch();
    }

    getSortFilter() {
        let styles = {
            sortListStyle: {
                width: '100%'
            },
            sortOrderListStyle: {
                width: '100%'
            },
            addButtonStyle: {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
            },
        }
        return (
            <div className="row">
                {this.state.filter.sort.map((sortItem, idx) => {
                    var items = [];
                    if (idx === 0) {
                        items.push(<MenuItem value="name_lat" primaryText="Latinského jména rostliny"/>);
                        items.push(<MenuItem value="name" primaryText="Českého jména rostliny"/>);
                        items.push(<MenuItem value="family_name" primaryText="Českého jména čeledi"/>);
                        items.push(<MenuItem value="family_name_lat" primaryText="Latinského jména čeledi"/>);
                    } else if (idx === 1) {
                        if (this.state.filter.sort[0].sortBy === name || this.state.filter.sort[0].sortBy === 'name_lat') {
                            items.push(<MenuItem value="family_name" primaryText="Českého jména čeledi"/>);
                            items.push(<MenuItem value="family_name_lat" primaryText="Latinského jména čeledi"/>);
                        } else {
                            items.push(<MenuItem value="name_lat" primaryText="Latinského jména rostliny"/>);
                            items.push(<MenuItem value="name" primaryText="Českého jména rostliny"/>);
                        }
                    }
                    return (
                        <div className="col-sm">
                            <div className="row">
                                <div className="col-sm-7">
                                    <SelectField
                                        disabled={idx + 1 < this.state.filter.sort.length}
                                        style={styles.sortListStyle}
                                        floatingLabelText="Řadit podle"
                                        value={this.state.filter.sort[idx].sortBy}
                                        onChange={(event, index, value) => {
                                            this.onSortByChange(idx, value)
                                        }}
                                    >
                                        {items}
                                    </SelectField>
                                </div>
                                <div className="col-sm-3">
                                    <SelectField
                                        disabled={idx + 1 < this.state.filter.sort.length}
                                        style={styles.sortOrderListStyle}
                                        floatingLabelText="Pořadí"
                                        value={this.state.filter.sort[idx].sortOrder}
                                        onChange={(event, index, value) => {
                                            this.onSortOrderChange(idx, value)
                                        }}
                                    >
                                        <MenuItem value="asc" primaryText="A-Z"/>
                                        <MenuItem value="desc" primaryText="Z-A"/>
                                    </SelectField>
                                </div>
                                {idx === this.state.filter.sort.length - 1 && this.state.filter.sort.length < 2 ?
                                    <div style={styles.addButtonStyle} className="col-sm-2">
                                        <ContentAddCircle onClick={this.onAddNewSortFilter}/>
                                    </div> : ''
                                }
                                {idx === this.state.filter.sort.length - 1 && this.state.filter.sort.length === 2 ?
                                    <div style={styles.addButtonStyle} className="col-sm-2">
                                        <ContentRemoveCircle onClick={this.onRemoveLastSortFilter}/>
                                    </div> : ''
                                }
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }

    onTagChange = (event, index, value) => {
        this.state.filter.tag = value;
        this.setState(this.state);
        this.updateSearch();
    }

    render() {
        //if (this.state.tableData && this.state.webp !== undefined) {
        let styles = {
            mainTable: {
                maxHeight: this.state.maxTableHeight,
                height: this.state.maxTableHeight,
                width: '100%'
            },
            tableBody: {
                height: this.state.scrollableHeight,
                position: 'relative'
            },
            tableBodyWrapper: {
                height: 500,
                overflowY: 'auto',
                overlfowX: 'none'
            },
            filterWrapper: {
                width: '100%',
                justifyContent: 'center',
                margin: 0
            },
            filterCard: {
                backgroundColor: 'rgba(245, 245, 220, 0.3)',
                width: '100%',
            },
            rootStyle: {
                fontFamily: 'arial'
            },
            resultsTag: {
                color: '#000000',
                borderRadius: 10,
                padding: '5px 10px 5px 10px',
                backgroundColor: '#e8e8e8'
            },
            tableHeader: {
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                height: 64
            }
        }

        return (
            <MuiThemeProvider>
                <div style={styles.rootStyle}>
                    <div className="row" style={styles.filterWrapper}>
                        <Card style={styles.filterCard}>
                            <CardText>
                                <div className="row">
                                    <div className="col-xs-12">
                                        <TextField
                                            value={this.state.filter.search}
                                            floatingLabelText="Heledaný text"
                                            onChange={this.onSearchChange}
                                            fullWidth={true}
                                        />
                                    </div>
                                </div>
                                {this.getSortFilter()}
                                <div className="row">
                                    <div className="col-xs-12">
                                        <SelectField
                                            floatingLabelText="Rostliny"
                                            value={this.state.filter.tag}
                                            onChange={this.onTagChange}
                                        >
                                            <MenuItem value="_ALL_" primaryText="všechny"/>
                                            <MenuItem value={11} primaryText="právě kvetou"/>
                                            <MenuItem value={185} primaryText="právě plodí"/>
                                        </SelectField>
                                    </div>
                                </div>
                                <div className="row">
                                    <FlatButton label="Zruš filtr" onClick={this.clearFilter}/>
                                </div>
                            </CardText>
                        </Card>
                    </div>
                    <div className="row">
                        <table style={styles.mainTable}>
                            <div style={styles.tableHeader}>
                                        {(!this.state.tableData || this.state.searchInProgress) ?
                                            <span style={styles.resultsTag}>Hledám...</span>
                                            :
                                            <span style={styles.resultsTag}>Nalezeno {this.state.tableData.length} záznamu</span>
                                        }
                            </div>
                            <div className="table-body" onScroll={this.handleScroll} style={styles.tableBodyWrapper}>
                                <div style={styles.tableBody}>
                                    {this.state.visibleRows}
                                </div>
                            </div>
                        </table>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

ReactDOM.render(<Widget/>, document.getElementById('widget'));