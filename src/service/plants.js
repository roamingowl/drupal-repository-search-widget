'use strict';
import Promise from 'bluebird';
import axios from 'axios';

export default class Plants {

    /**
     * @constructor
     * @param {String} apiEndpointUrl
     */
    constructor(apiEndpointUrl) {
        this.apiEndpointUrl = apiEndpointUrl;
        this.plantsCollection = null;
    }

    /**
     *
     * @returns {Promise}
     */
    getCollection() {
        if (this.plantsCollection) {
            return Promise.resolve(this.plantsCollection)
        }
        return axios.post(this.apiEndpointUrl, {})
            .then(function (result) {
                return result.data;
            })
    }
}
